<?php

	if(isset($_POST["a"]))
	{
		$a = (float)$_POST["a"];
	}

	if(isset($_POST["k"]))
	{
		$k = $_POST["k"];
	}

	if($a != 0 && $a > 0)
	{

		try {

			// Requested file
			// Could also be e.g. 'currencies.json' or 'historical/2011-01-01.json'
			$file = 'latest.json';
			$appId = $k;

			// Open CURL session:
			$ch = curl_init("http://openexchangerates.org/api/{$file}?app_id={$appId}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);

			// Decode JSON response:
			$exchangeRates = json_decode($json);

			$price = $exchangeRates->rates->BTC * $a;
								 
			echo  "&#579;" . $price . " BTC";
				
			
		} catch (Exception $e) {
			
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	
	}
	else
	{
		echo "Invalid Input";
	}
	
	
?>