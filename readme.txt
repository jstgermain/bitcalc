=== BitCoin Calculator ===

Contributors: Justin St. Germain
Tags: bitcoin, currency, converter, calculator, usd, btc, openexchangerates
Requires at least: 3.0.1
Tested up to: 4.1.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Use BitCoin Calc as a sidebar widget or shortcode entry to display a USD to Bitcoin conversion rates provided from openexchangerates.org API.
Plugin uses live data from openexchangerates.org to convert USD to Bitcoin currency.
Plugin data gathers data from API found at http://openexchangerates.org/

== Installation ==

1. Upload contents of bit-calc to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use the widgets menu on your wordpress theme to add the widget to the sidebar, or the shortcode entry '[bit-calc]' to display the converter on any page or a post.

== Changelog ==
= 1.0.0 =
* Initial release.

== Upgrade Notice ==
= 1.0.0 =
* Initial release.