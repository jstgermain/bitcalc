<?php

/**
 * BitCalc Currency Converter
 *
 * Convert USD to BitCoin using openexchangerates.org API
 *
 * Plugin Name: BitCalc Currency Converter
 * Plugin URI: http://www.ibrightdev.com
 * Description: Use this plugin to convert USD to BTC. You can create a widget on the sidebar or use the shortcode ([bit-calc]) to display the calculator on any page
 * Author: Justin St. Germain
 * Version: 1.0.0
 * Author URI: http://www.ibrightdev.com
 */


/**
 * Global Variables
 */

global $wp_roles;

register_activation_hook(__FILE__, 'bit_calc_plugin_install');
register_deactivation_hook( __FILE__, 'bit_calc_plugin_unininstall' );


/**
 * INSTALL/UNINSTALL
 */

add_action( 'wp_head', 'bit_calc_head_css_jq' );
function bit_calc_head_css_jq()
{
	$url = plugins_url();
}


/**
 * Add settings link on plugin page
 */
function your_plugin_settings_link($links) {
	$settings_link = '<a href="options-general.php?page=bit_calc_settings_page">Settings</a>';
	array_unshift($links, $settings_link);
	return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'your_plugin_settings_link' );


/**
 * Add settings on activate.
 */

add_action('admin_init', 'bit_calc_plugin_install');
function bit_calc_plugin_install()
{
	register_setting('bit_calc_options', 'bit_calc_options');
}


/**
 * Unregister settings on deactivate.
 */

add_action('admin_init', 'bit_calc_plugin_unininstall');
function bit_calc_plugin_unininstall ()
{
	unregister_setting('bit_calc_options', 'bit_calc_options');
}


/**
 * Create admin page.
 */

add_action('admin_menu','create_bit_calc_admin');
function create_bit_calc_admin ()
{
	$mypage = add_options_page("BitCalc Options", "BitCalc Settings", 10, "bit_calc_settings_page", "bit_calc_html");
}


/**
 * Print the API Key setting form field.
 * Plugin settings field callback function.
 */
function print_api_key_field() {

	$option = get_option( 'bit_calc_options' );
	$api_key = isset( $option['api_key'] ) ? esc_attr( $option['api_key'] ) : ''; ?>

	<label for="api_key">
		<?php _e( 'Open Exchange Rates API key:', 'wp_bit_calc' ); ?>
		<input type="password" id="api_key" name="bit_calc_options[api_key]" value="<?php echo $api_key; ?>" class="regular-text">
	</label>
	<br>
	<small>
		<?php printf(
			_x( 'Get yours at: %1s', 'URL where to get the API key', 'wp_bit_calc' ),
			'<a href="//openexchangerates.org/" target="_blank">openexchangerates.org</a>' ); ?>
	</small>
<?php

}


/**
 * Feed settings
 */
add_action('admin_init', 'add_bit_calc_options');
function add_bit_calc_options()
{
	add_settings_section('bit_calc_section', 'BitCalc Settings', 'sctn_bit_calc', 'bit_calc_settings_page');
	add_settings_field('bit_calc_dropdown_currency', 'Default calculator currency', 'print_api_key_field', 'bit_calc_settings_page', 'bit_calc_section',$args=array("name"=>"btcchina"));
}

function sctn_bit_calc ()
{
	echo "<p>Choose which currency to use on the converter by default.</p>";
}


/**
 * Create widget from plugin for sidebar.
 */

function widget_bit_calc($args)
{
	bit_calc_widget();
}

function bitCalcWidget_init()
{
	register_sidebar_widget(__('BitCalc Widget'), 'widget_bit_calc');
}


/**
 * Shortcode
 */

add_action("plugins_loaded", "bitCalcWidget_init");
function bit_calc_fn( $attributes )
{
	return bit_calc_widget();
}

add_shortcode( 'bit-calc','bit_calc_fn' );


include('bit-calc-admin.php');
include('bit-calc-widget.php');
