<?php
	function bit_calc_widget()
	{
		
		$calculator_options = get_option('wp_bit_calc_options');
		$url = plugins_url();

		$option = get_option( 'bit_calc_options' );
		$api_key = isset( $option['api_key'] ) ? esc_attr( $option['api_key'] ) : '';

	?>	
		
		<div class="bit-calc">
			<div class="heading">
				BitCalc Currency Converter
			</div>
			<div class="sub-heading">
				Amount of USD to convert to BTC
			</div>
			<div id="bit-calc-ammount">
				<input type="number" value="1" type="text" id="ammount-to-convert"/>
			</div>
			<div class="bit-calc-exchanged">
				<span class="btc-converted-amount"></span>
			</div>
		</div>

		<script type="text/javascript">
			var j = jQuery.noConflict();
			j(function(){
				convert ();
				j('#ammount-to-convert').change(function(){
					j('#ammount-to-convert').trigger("keyup");
				});
				j('#ammount-to-convert').keyup(function(){
					convert ();
				});
			});

			function convert ()
			{
				var url = '<?php echo bloginfo('wpurl'); ?>' + '/wp-content/plugins/bit-calc/exchange.php';
				var a = j('#ammount-to-convert').val();
				var k = '<?php echo $api_key; ?>';
				var data = "a="+a +"&k="+k;
				j.ajax({
					type: 'POST',
					url: url,
					data: data,
				}).success(function(d) {
					j('.btc-converted-amount').html(d);
				});
			}
		</script>
		
	<?php
	}
