<?php

function bit_calc_html ()
{

	echo    '<h2>BitCalc Currency Converter</h2>'
		.   '<div>'
		.   '<form action="options.php" method="post">'
	;

	settings_fields('bit_calc_options');
	do_settings_sections('bit_calc_settings_page');

	echo    '<p class="submit">'
		.   '<input name="Submit" type="submit" class="button-primary" value="Save Changes" />'
		.   '</p>'
		.   '</form>'
		.   '</div>'
	;

}
